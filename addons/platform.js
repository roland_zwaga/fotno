'use strict';

var path = require('path'),
	madge = require('madge'),

	arrayUnique = require('../primitives/arrayUnique');

module.exports = function (app) {
	app.registerAddon('platform', [
		PlatformAddon
	]);

	function PlatformAddon (code) {

	}

	PlatformAddon.prototype.refresh = function (code) {
		this._dependencies = madge(path.join(code.path), {
			format: 'amd',
			findNestedDependencies: true
		});
	};

	PlatformAddon.prototype.getPlatformDependencies = function (fileNameFilter) {
		return arrayUnique(Object.keys(this._dependencies.tree).reduce(function (allDependencies, fileName) {
			var platformDependencies = this._dependencies.tree[fileName].filter(function (dependency) {
					if (dependency.indexOf('fontoxml-') !== 0) {
						return false;
					}

					if (fileNameFilter) {
						return fileNameFilter(fileName + '.js');
					}

					return true;
				});

			return allDependencies.concat(platformDependencies);
		}.bind(this), []));
	};

	PlatformAddon.prototype.init = function (code) {
		return this.refresh(code);
	};
};
