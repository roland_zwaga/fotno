'use strict';
const path = require('path'),
	os = require('os'),
	fs = require('fs-extra'),
	AskNicely = require('ask-nicely'),

	gitDownload = require('../../primitives/gitDownload'),
	runPromisesInParallel = require('../../primitives/runPromisesInParallel'),
	getStatForPath = require('../../primitives/getStatForPath'),
	getAllStatsInPath = require('../../primitives/getAllStatsInPath');

function createSymbolicLink (sourceLocation, targetLocation) {
	return new Promise((resolve, reject) => {
		fs.ensureSymlink(sourceLocation, targetLocation, 'dir', (err) => err ? reject(err) : resolve(true));
	});
}

module.exports = app => {
	app.cli.addCommand('link')
		.setDescription(`(Experimental) Override or replace packages with symbolic links to copies on your filesystem.`)
		.setLongDescription(`If you have access to the BitBucket repositories you can use this for core development or unminified code. Only removes directories from the destination path if they're already in the source path`)
		.addOption('verbose', 'v', 'Print a shitload of information on what\'s going on')
		.addOption('porcelain', 'p', 'Do not replace existing directories in destination')
		.addOption('relink', 'R', 'Replace existing symlinks')
		.addOption('include-vendor', false, 'Include linking (but mostly downloading) vendor dependencies')
		.addOption('dry', 'D', 'Simulated run, do not actually change anything on the filesystem.')
		.addOption('source', 's', 'Path containing source code to packages, absolute or relative from the current working directory.')
		.addOption('mirror', 'm', 'Path that you want to mirror, absolute or relative from the current working directory.')
		.addOption('destination', 'd', 'Destination override, absolute or relative from the current working directory.')
		.addOption('download', null, 'Attempt to download repository from Liones\' Bitbucket repositories if source doesn\'t exist. Requires SSH access.')
		.addExample('fotno link --download 6.3.0', 'Creates symbolic links in platform-linked/ for every package found in platform/, and download the package versions belonging to SDK 6.3.0 to ../../libs/.')
		.addExample('fotno link --mirror packages-shared --source /git/fontoxml/contrib', 'Replaces everything in packages-shared/ that has an equivalent in /git/fontoxml/contrib with a symbolic link to that location.')
		.addExample('fotno link --destination platform --download 6.2.0', 'Replace everything in the platform/ directory with an unminified copy. The destination flag is a useful override for applications on SDK < 6.3.0.')
		.setController(bigFuckingChunkOfCode);

	function bigFuckingChunkOfCode (req, res) {
		let code = req.scope[0],
			verbose = !!req.options.verbose,
			porcelain = !!req.options.porcelain,
			overwrite = !porcelain && !!req.options.relink,
			sdkVersion = typeof req.options.download === 'string' ? req.options.download : 'develop',
			download = !!req.options.download,
			dry = !!req.options.dry,
			source = req.options.source || path.resolve(code.path, '..', '..', 'libs'),
			sourcePath = path.resolve(app.processPath, source),

			mirror = req.options.mirror || path.resolve(code.path, 'platform'),
			mirrorPath = path.resolve(app.processPath, mirror),
			mirrorPlatform = mirrorPath === path.resolve(code.path, 'platform'),

			destination = req.options.destination || (mirrorPath === path.resolve(code.path, 'platform')
				// if supplanting platform, use platform-linked as default destination
				? path.resolve(code.path, 'platform-linked')
				: mirrorPath),
			destinationPath = path.resolve(app.processPath, destination),
			logStatuses = {},
			promise = Promise.resolve(getAllStatsInPath.sync(mirrorPath, false)
				.filter(stat => !stat.file)
				.map(stat => Object.assign(stat, { packageName: path.basename(stat.path) })));

		function logStatus(status, packageName, formatting) {
			if(logStatuses[status])
				++logStatuses[status];
			else
				logStatuses[status] = 1;
			if(verbose || formatting === 'error')
				res.property(status, packageName, 14, formatting);
		}

		res.caption(`fotno link`);


		promise = promise.then(stats => {
			res[verbose ? 'caption' : 'debug'](`Checking source & destination files (${stats.length})`);

			!stats.length && verbose && res.debug('Nothing to check.');

			return stats
				.filter(stat => {

					if(!(mirrorPlatform && stat.packageName.indexOf('fontoxml-vendor-') === 0) || req.options['include-vendor'])
						return true;

					logStatus('Ignore vendor', stat.packageName, 'notice');

					return false;
				})
				.filter(stat => {
					stat.destination = path.resolve(destinationPath, stat.packageName);
					try {
						stat.alreadyLinked = getStatForPath.sync(stat.destination, false).symlink || true;
					} catch (e) {
						stat.alreadyLinked = false;
						return true;
					}

					if (overwrite)
						return true;

					if (stat.alreadyLinked === true && !porcelain)
						return true;

					if (stat.alreadyLinked === true && porcelain)
						logStatus('Already exists', stat.destination);
					else
						logStatus('Already linked', stat.alreadyLinked);

					return false;
				})
				.filter(stat => {
					stat.source = path.resolve(sourcePath, stat.packageName);

					try {
						getStatForPath.sync(stat.source);
						stat.alreadySourced = true;
					} catch (e) {
						stat.alreadySourced = false;
					}

					if (!stat.alreadySourced)
						logStatus('No source', stat.source, download ? 'log' : 'error');

					return download || stat.alreadySourced;
				});
		});

		// Download what isnt in source already
		if(download)
			promise = promise.then(stats => {
				res[verbose ? 'caption' : 'debug'](`Retrieving platform tags for release ${sdkVersion} (${stats.length})`);

				if(!mirrorPlatform) {
					verbose && res.debug('Not downloading tags because not mirroring platform.');
					return stats;
				}

				if(!stats.filter(stat => !stat.alreadySourced).length) {
					verbose && res.debug('No release tags needed because nothing to download.');
					return stats;
				}

				return code.getAddon('sdk').getTagsForVersion(sdkVersion)
					.then(resolvedPackageTags => stats.map(stat => Object.assign(stat, {
						head: resolvedPackageTags[stat.packageName] || 'develop'
					})))
					.then(stats => {
						if(verbose)
							res.properties(stats.map(stat => [stat.head, stat.packageName]));
						return stats;
					});
			})
			.then(stats => {
				res[verbose ? 'caption' : 'debug'](`${dry ? 'Dry downloading' : 'Downloading'} source packages (${stats.filter(stat => !stat.alreadySourced).length})`);

				!stats.length && verbose && res.debug('Nothing to download.');

				return runPromisesInParallel(stats.map(stat => () => {
						if(stat.alreadySourced)
							return stat;

						let downloadOptions = {
							source: `ssh://git@bitbucket.org/liones/${stat.packageName}.git`,
							dest: stat.source,
							branch: stat.head || 'develop'
						};

						logStatus(dry ? 'Dry try download' : 'Try Download', downloadOptions.source);

						return (dry ? Promise.resolve(stat) : gitDownload(downloadOptions))
							.then(() => {
								logStatus(dry ? 'Dry downloaded' : 'Downloaded', stat.source);
								stat.alreadySourced = true;
								return stat;
							})
							.catch(err => {
								console.log(err);
								stat.error = err;
								logStatus('Download error', stat.packageName +' ' + (err.message|| '').trim(), 'error');

								return stat;
							});
					}));
			})
			.then(stats => stats.filter(stat => stat.alreadySourced));

		// Check if the package is already in the destination directory
		promise = promise.then(stats => {
				res[verbose ? 'caption' : 'debug'](`${dry ? 'Dry removing' : 'Removing'} destination clutter (${stats.length})`);

				!stats.length && verbose && res.debug('Nothing to remove.');

				return stats.map(stat => {
					// If theres nothing to remove, do not remove
					// If porcelain mode, never remove anything
					// Do not remove if there is no source already available
					if (!stat.alreadyLinked || porcelain)
						return stat;

					// If destination is a directory, or if relinking
					if (stat.alreadyLinked === true || overwrite) {
						logStatus(dry ? 'Dry remove' : 'Remove', stat.destination);
						!dry && fs.removeSync(stat.destination);
						stat.wasRemoved = true;
					}

					return stat;
				});
			});

		promise = promise.then(stats => {
				res[verbose ? 'caption' : 'debug'](`${dry ? 'Dry creating' : 'Creating'} symbolic links to source (${stats.length})`);

				!stats.length && verbose && res.debug('Nothing to link.');

				return Promise.all(stats.map(stat => dry
					? !logStatus('Dry linked', stat.source, stat.wasRemoved ? 'info' : 'log') && stat.packageName
					: createSymbolicLink(stat.source, stat.destination)
						.then(() => {
							logStatus('Linked', stat.source, stat.wasRemoved ? 'info' : 'log');
							return stat;
						})
						.catch((err) => {
							logStatus('Link error', stat.packageName +' ' + err.message.trim(), 'error');
							stat.error = err;
							return stat;
						})
				))
			})
			.then(stats => stats.filter(stat => !stat.error));

		promise = promise.then(stats => {
				res.break();
				res.success(`All done (${stats.length}): ` + Object.keys(logStatuses)
					.map(key => `${logStatuses[key]} ${key.toLowerCase()}`)
					.join(', ') || 'nothing to report');
				res.break();
				res.properties({
					'Method': (download && 'Download') || 'Existing sources only',
					'Mode': (porcelain && 'Porcelain (replace nothing)') || (overwrite && 'Relink (replace anything)') || 'Normal (replace directories)',
					'Source' : sourcePath,
					'Destination': path.resolve(code.path, destination)
				});
			});

		return promise;
	}
};