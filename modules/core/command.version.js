'use strict';

const path = require('path');
const AskNicely = require('ask-nicely');
const packageJson = require(path.resolve(__dirname, '..', '..', 'package.json'));

module.exports = app => {
	app.cli.addOption(new AskNicely.IsolatedOption('version')
			.setShort('v')
			.setDescription('Show version information for fotno'))
			.addPreController((req, res) => {
				if(req.command !== app.cli || !req.options.version)
					return;

				console.log(packageJson.version);

				return false;
	});
};