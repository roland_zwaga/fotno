'use strict';

const path = require('path'),
	fs = require('fs-extra'),
	os = require('os');

module.exports = app => {
	function helpController(req, res) {
		var logos = fs.readFileSync(path.resolve(__dirname, 'logos.txt')).toString().split(os.EOL),
			seperate = logos.reduce((all, line) => {
				if(line)
					all[0].push(line);
				else if (all[0].length)
					all.unshift([]);
				return all;
			}, [[]])
			.filter (lines => lines.length);

		res.break();
		console.log(seperate[0]
			.map(line => '  ' + line)
			.join(os.EOL));
		res.break();
		res.debug('v' + require(path.resolve(__dirname, '..', '..', 'package')).version);
	}

	app.cli.setController(helpController);
};