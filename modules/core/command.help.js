'use strict';

const path = require('path');
const AskNicely = require('ask-nicely');
const NO_DESCRIPTION = '<no description>';
// the package.json of fotno/fotno
const packageJson = require(path.resolve(__dirname, '..', '..', 'package.json'));

function sortByName(a, b) {
	return a.name < b.name ? -1 : 1;
}
function toCommandRow (cmd) {
	return [
		cmd.name,
		(cmd.description || NO_DESCRIPTION) + '\n'
	];
}
function toParameterRow (param) {
	return [
		`<${param.name}>`,
		(param.description || NO_DESCRIPTION) + (param.required ? ' [required]' : '')
	];
}
function toOptionRow (option) {
	return [
		(option.short ? `-${option.short}` : '--') + `  --${option.name}`,
		(option.description || NO_DESCRIPTION) + (option.required ? ' [required]' : '')
	];
}


module.exports = app => {
	app.cli.setDescription(packageJson.description);

	function helpController(req, res) {
		var command = req.command,
			isRoot = !command.parent;

		res.caption(`fotno help`);
		var props = [];
		(command.description || command.aliases.length) && props.push(['Command', command.name]);
		command.aliases.length && props.push(['Aliases', command.aliases.join(', ')]);
		command.description && props.push(['Summary', command.description]);

		if(props.length) {
			res.properties(props);
		}
		if (command.longDescription) {
			res.break();
			res.debug(command.longDescription);
		}

		if(command.children.length) {
			res.caption('Child commands');
			command.children.sort(sortByName).forEach(child => res.definition(child.name, child.description));
		}

		if(command.parameters.length) {
			res.caption('Parameters');
			res.properties(command.parameters.map(toParameterRow));
		}

		var options = command.options;
		if(options.length) {
			res.caption('Options');
			res.properties(options.sort(sortByName).map(toOptionRow));
		}

		var examples = command.examples;
		if(examples && examples.length) {
			res.caption('Examples');
			examples.forEach(fuckMartin => {
				res.definition(fuckMartin.caption, fuckMartin.content);
			})
		}
	}

	app.cli.addOption(new AskNicely.IsolatedOption('help')
		.setShort('h')
		.setDescription('Show usage information for this command'))
		.addPreController((req, res) => {
			if(!req.options.help)
				return;

			helpController.call(this, req, res);

			return false;
		});
};