'use strict';

const os = require('os');
const path = require('path');

module.exports = app => {
	app.cli.addCommand('who', (req, res) => {
			res.caption('fotno who');
			res.properties({
				'Hostname': os.hostname(),
				'Install': path.resolve(__dirname, '..', '..'),
				'Config': app.config.getPath() || '-',
				'Modules': app.config.modules.join(', '),
				'Commands': app.cli.children.map(command => command.name).join(', ')
			});

			res.caption('Scope');
			req.scope.forEach(function (code) {
				res.properties({
					'Application': code.getAddon('identity').name,
					'SDK version': code.getAddon('identity').sdkVersion,
					'Path': code.path
				});
			});

			res.caption('Schema locations');
			req.scope.forEach(code => {
				var schemaInfo = code.getAddon('schema').getSchemaSummaries();
				Object.keys(schemaInfo).sort().forEach(schemaName => {
					res.definition(schemaName, schemaInfo[schemaName].locations
						? schemaInfo[schemaName].locations.sort().join('\n')
						: 'No schema locations detected');
				});
			});
		})
		.setDescription(`Tells you what you are, and what you're doing here.`)
		.addAlias('whoami');
};