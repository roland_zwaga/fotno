'use strict';

var path = require('path');
var AskNicely = require('ask-nicely');

const Code = require('../../classes/Code');
const getParentDirectoryContainingFile = require('../../primitives/getParentDirectoryContainingFile'),
	DEFAULT_ADDONS_TODO_FIX_ME = [
		'identity',
		'cms',
		'sdk',
		'operations',
		'toolbar',
		'schema'
	];
module.exports = app => {

	app.cli.addPreController((req, res) => {
		var manifestlocation = getParentDirectoryContainingFile.sync(app.processPath, 'manifest.json');

		if(!manifestlocation) {
				// throw new AskNicely.InputError(
				// 	`Could not find the editor repository`,
				// 	`Try running fotno from anywhere within a FontoXML application repository.`);
			res.break();
			res.notice('You\'re not running fotno from inside an editor repository; this means fotno may not work as expected!');
		}

		// fotno and fotno are siblings, designed for use on single applications, the other on collections of
		// repositories they both work with the array req.scope
		req.scope = [new Code(manifestlocation || app.processPath)];

		return Promise.all(req.scope.map(code => code.init(Code.getAddonSubset(DEFAULT_ADDONS_TODO_FIX_ME))));
	})
};