'use strict';

var path = require('path');

module.exports = app => {
	app.registerAddon('identity', [
		IdentityAddon
	]);
};

function safeReadJson (path) {
	try {
		return require(path);
	} catch(e) {
		return false;
	}
}

class IdentityAddon {
	constructor (code) {
		Object.assign(this, safeReadJson(path.join(code.path, 'manifest.json')));
		this.bower = safeReadJson(path.join(code.path, 'bower.json'));
		this.npm = safeReadJson(path.join(code.path, 'package.json'));
		this.config = safeReadJson(path.join(code.path, 'config.json'));
	}

	init (code) {

	}
}


