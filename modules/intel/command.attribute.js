'use strict';

var path = require('path');

module.exports = app => {
	function attributeCommand(req, res) {
		var schemaAddon = req.scope[0].getAddon('schema'),
			schemaLocation = schemaAddon.getClosestMatchingSchemaLocation(req.options.schema),
			attributeName = null,
			elementName = null;

		if(req.parameters.attribute.indexOf('@') >= 0) {
			elementName = req.parameters.attribute.split('@')[0];
			attributeName = req.parameters.attribute.split('@')[1];
		} else {
			attributeName = req.parameters.attribute
		}

		var attributeSummaries = schemaAddon.getAttributeSummary(
				schemaLocation,
				attributeName);

		res.caption('fotno attribute');
		res.debug('Reading from ' + schemaLocation);

		res.debug(`Found ${attributeSummaries.length} attribute definitions for attribute "${attributeName}" across ${attributeSummaries.reduce((tot, sum) => tot + sum.elements.length, 0)} elements.`);
		elementName && res.debug(`Filtering definitions applicable to <${elementName}>`);

		if(elementName) {
			attributeSummaries = attributeSummaries
				.filter(attrDefinition => attrDefinition.elements && attrDefinition.elements.indexOf(elementName) >= 0);
		}

		if(!attributeSummaries.length)
			throw new Error(`Looks like there is no "${attributeName}" attribute definition that matches your criteria.`);

		attributeSummaries.forEach(function (attrDefinition, index) {
			res.caption('Definition ' + (index + 1));
			res.properties({
				'localName': attrDefinition.localName,
				'Type localName': attrDefinition.typeLocalName,
				'Type namespace': attrDefinition.typeNamespace || '-',
				'Use': attrDefinition.use || '-',
				'Default value': attrDefinition.defaultValue || '-',
				'Allowed values': attrDefinition.allowedValues.length
					? attrDefinition.allowedValues.join(', ')
					: '-',
				'Elements': attrDefinition.elements.length
					? attrDefinition.elements.join(', ')
					: '-'
			});
		});
	}

	app.cli.addCommand('attribute', attributeCommand)
		.addParameter('attribute', 'The name of an attribute. Use either attributeName or elementName@attributeName', true)
		.addOption('schema', 's', 'Specify (a part of) the path to the source schema.json', false)
		.setDescription('Get schema information about a specific attribute. An attribute may have different definitions in the same schema, this command summarizes them all.')
		.setLongDescription('An attribute may have different definitions per schema.json because an attribute of the same name may have different valid options, defaults or data type in other elements.')
		.addExample('fotno attribute id', 'Print information about all attribute definitions for @id if the application has only one schema.json')
		.addExample('fotno attribute id -s map', 'Print information about all attribute definitions for "id" if the application has one shell in a package named something with "map"')
		.addExample('fotno attribute ol@class -s topic', 'Print information about the @class attribute on <ol> in a DITA shell matching "topic"');
};
