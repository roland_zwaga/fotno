'use strict';

const path = require('path');
const Table = require('../../helpers/TableCommand');

module.exports = app => {
	var table = new Table([
		{
			name: 'name',
			default: true,
			label: 'Name',
			value: (element) => element.localName
		},
		{
			name: 'desc',
			default: true,
			label: 'Description',
			value: (element) => element.documentation
		},
		{
			name: 'cnt',
			label: 'Content model',
			value: (element) => element.contentModelString
		},
		{
			name: 'empty',
			label: 'Empty',
			value: (element) => element.isEmpty
		},
		{
			name: 'mixed',
			label: 'Mixed',
			value: (element) => element.isMixed
		},
		{
			name: 'abstract',
			label: 'Abstract',
			value: (element) => element.isAbstract
		},
		{
			name: 'children',
			label: 'Contains',
			value: (element) => element.contains.join(', ')
		},
		{
			name: 'parents',
			label: 'Contained by',
			value: (element) => element.containedBy.join(', ')
		},
		{
			name: 'attrs',
			label: 'Attributes',
			value: (element) => element.attributes.map(attr => attr.localName + (attr.use === 'required' ? '*' : '')).join(', ')
		}
	]);
	
	function elementCommand(req, res) {
		var schemaAddon = req.scope[0].getAddon('schema');

		res.caption('fotno elements');

		table.print(
			res,
			req.options.columns,
			schemaAddon.getElementSummaries(schemaAddon.getClosestMatchingSchemaLocation(req.options.schema)),
			req.options.sort,
			req.options.export
		);
	}

	app.cli.addCommand('elements', elementCommand)
		.addAlias('list-elements')
		.addOption('schema', 's', 'In a multi-schema app, specify (a part of) the path to the source schema.json', false)
		.addOption(table.sortOption)
		.addOption(table.columnsOption)
		.addOption(table.exportOption)
		.setDescription('Print a table with information about all elements in a schema.');
};
