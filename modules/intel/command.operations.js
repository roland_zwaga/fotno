'use strict';

const path = require('path');
const Table = require('../../helpers/TableCommand');

module.exports = app => {
	var table = new Table([
		{
			name: 'name',
			default: true,
			label: 'Name',
			value: (operation) => operation.operationName
		},
		{
			name: 'file',
			default: true,
			label: 'File',
			value: (operation) => operation.operationFile
		},
		{
			name: 'label',
			default: true,
			label: 'Label',
			value: (operation) => operation.label
		},
		{
			name: 'desc',
			label: 'Description',
			value: (operation) => operation.description
		},
		{
			name: 'key',
			label: 'Key binding',
			value: (operation) => operation.keyBinding
		}
	]);

	function operationsCommand (req, res) {
		return Promise.all(req.scope.map(code => code.getAddon('operations').getOperations()))
			.then(allOperations => {
				res.caption('fotno operations');
				table.print(
					res,
					req.options.columns,
					allOperations.reduce((flatList, codeOperations) => flatList.concat(codeOperations)),
					req.options.sort,
					req.options.export
				);
			})
	}
	
	app.cli.addCommand('operations')
		.addAlias('list-operations')
		.setDescription('Print a table with summarized info on all the operations in a repository.')
		.setController(operationsCommand)
		.addOption(table.exportOption)
		.addOption(table.columnsOption)
		.addOption(table.sortOption);

};