'use strict';

var path = require('path');

function stringForEmptyOrMixed (empty, mixed) {
	if(empty && mixed)
		return 'Nothing';
	if(mixed)
		return 'Text only';
	if(empty)
		return 'Elements only';

	return 'Text and elements';
}

module.exports = app => {
	function elementCommand(req, res) {
		var schemaAddon = req.scope[0].getAddon('schema'),
			elementSummary = schemaAddon.getElementSummary(
				schemaAddon.getClosestMatchingSchemaLocation(req.options.schema),
				req.parameters.element);

		res.caption('fotno element');
		res.debug('Reading from ' + elementSummary.schemaFile);
		res.break();

		let summary = {
				'Element name': elementSummary.localName,
				'Documentation': elementSummary.documentation || '-',
				'Contents': stringForEmptyOrMixed(elementSummary.isEmpty, elementSummary.isMixed),
				//'Mixed content': elementSummary.isMixed ? 'Yes' : 'No',
				//'Abstract': elementSummary.isAbstract ? 'Yes' : 'No',
				'Contained by':  elementSummary.containedBy.length + ' unique nodes',
				'Contains': elementSummary.contains.length + ' unique nodes',
				'Attributes': elementSummary.attributes.length + ' unique attributes',
				'Default class': elementSummary.attributes
					.filter(attr => attr.localName === 'class')
					.map(attr => attr.defaultValue)
					.filter(classAttr => !!classAttr).join(', ') || '-'
			};

		res.properties(summary);

		if(elementSummary.contentModelString) {
			res.caption('Content model');
			res.debug(elementSummary.contentModelString);
		}

		if(elementSummary.contains.length) {
			res.caption('Contains...');
			res.debug(elementSummary.contains.join(', '));
		}

		if(elementSummary.containedBy.length) {
			res.caption('Contained by...');
			res.debug(elementSummary.containedBy.join(', '));
		}
		
		if(elementSummary.attributes.length) {
			res.caption('Attributes...');
			res.debug(elementSummary.attributes.map(attr => attr.localName + (attr.use === 'required' ? '*' : '')).join(', '));
		}
	}

	app.cli.addCommand('element', elementCommand)
		.addParameter('element', 'Query a specific element', true)
		.addOption('schema', 's', 'Specify (a part of) the path to the source schema.json', false)
		.setDescription('Get schema information about a specific element, including detailed content model info.')
		.addExample('fotno element article', 'Print information about the <article> element if the application has only one schema.json')
		.addExample('fotno element topicref -s map', 'Print information about the <topicref> element if the application has one shell in a package named something with "map"')
		.addExample('fotno element body -s packages/non-standard-schema-package/src/data.json', 'Print information about the <body> element from a schema.json in a non-standard location');
};
