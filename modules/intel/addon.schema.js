'use strict';

const AskNicely = require('ask-nicely'),
	Requirejs = require('requirejs'),
	fs = require('fs'),
	path = require('path'),
	glob = require('globby'),
	arrayUnique = require('../../primitives/arrayUnique');

function getSchemaLocations (code, packageRoot) {
	return glob.sync(['**/schema.json'], {
		cwd: path.resolve(code.path, packageRoot)
	})
}
module.exports = app => {

	class SchemaAddon {
		constructor(code) {
			this._code = code;
		}

		init (code) {

		}

		getSchemaSummaries (readSchemaData) {
			let noSchemaData = !readSchemaData,
				basePath = path.resolve(this._code.path),
				requirejs = Requirejs(({
					paths: {
						text: path.resolve(this._code.path, 'platform/fontoxml-vendor-requirejs-text/text'),
						json: path.resolve(this._code.path, 'platform/fontoxml-vendor-requirejs-plugins/src/json')
					}
				}));
			return glob.sync([
					'packages/**/schema.json',
					'packages/*/src/SCHEMA_DATA.*',
					'packages/*/src/SCHEMA_LOCATIONS.*'
				], {
					cwd: basePath
				})
				.reduce((app, location) => {
					var type = path.basename(location).split('.')[0],
						packageName = location.split('/').splice(0,2).join('/');

					if(!app[packageName])
						app[packageName] = {};

					switch(type) {
						case 'schema':
							if(noSchemaData)
								app[packageName].data = location;
							else
								app[packageName].data = require(path.resolve(basePath, location));
							return app;
						case 'SCHEMA_DATA':
							if(noSchemaData)
								app[packageName].data = location;
							else if(!app[packageName].data)
								app[packageName].data = requirejs(path.resolve(basePath, location));
							return app;
						case 'SCHEMA_LOCATIONS':
						default:
							app[packageName].locations = requirejs(path.resolve(basePath, location));
							return app;
					}
				}, {});
		}

		getClosestMatchingSchemaLocation (searchString) {
			try {
				fs.statSync(path.resolve(app.processPath, searchString));
				return path.relative(this._code.path, searchString);
			} catch (e) {
				// searchString is not an existing file
			}

			var schemaLocations = getSchemaLocations(this._code, 'packages'),
				results = (schemaLocations.length === 1 || !searchString)
					? schemaLocations
					: schemaLocations.filter(schemaLocation => schemaLocation.indexOf(searchString) >= 0);

			if(!schemaLocations.length)
				throw new AskNicely.InputError(
					`Couldn't find a schema.json in this application.`,
					`Use the schema compiler on http://sdk.fontoxml.com to obtain the optimized schema.json file for your XSD's, or type the full path to the schema JSON relative to your current working directory.`
				);

			if(results.length < 1)
				throw new AskNicely.InputError(
					`Couldn't find a schema matching with "${searchString}"`,
					`Try to find an unambiguous short for one of the following:\n\t- ${schemaLocations.map(sL => sL.split('/')[0]).join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
				);

			if(results.length > 1)
				if(searchString)
					throw new AskNicely.InputError(
						`The string "${searchString}" yielded more than one results.`,
						`Try to find an unambiguous short for one of the following:\n\t- ${schemaLocations.map(sL => sL.split('/')[0]).join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
					);
				else
					throw new AskNicely.InputError(
						`This application has more than one schema, you must use the --schema option to specify (a part of) the package name.`,
						`Try to find an unambiguous short for one of the following:\n\t- ${schemaLocations.map(sL => sL.split('/')[0]).join('\n\t- ')}\nOr type the full path to the schema JSON relative to your current working directory.`
					);

			return path.join('packages', results[0]);
		}

		getElementSummary (schemaLocation, nodeName) {
			var schemaFileName = path.join(this._code.path, schemaLocation),
				schemaFile = require(schemaFileName),
				nodeDefinition = schemaFile.elements.find(nodeDef => nodeDef.localName === nodeName);

			if(!nodeDefinition)
				throw new Error(`The "${nodeName}" element does not exist in this schema.`);

			return Object.assign(getElementSummary(schemaFile, nodeDefinition), {
				schemaFile: schemaFileName.substr(this._code.path.length + 1)
			});
		}

		getElementSummaries (schemaLocation) {
			var schemaFileName = path.join(this._code.path, schemaLocation),
				schemaFile = require(schemaFileName);

			return schemaFile.elements
				.map(getElementSummary.bind(undefined, schemaFile))
				.map(elementSummary => Object.assign(elementSummary, {
					schemaFile: schemaFileName.substr(this._code.path.length + 1)
				}));
		}

		getAttributeSummary (schemaLocation, attrName) {
			var schemaFileName = path.join(this._code.path, schemaLocation),
				schemaFile = require(schemaFileName),
				attrDefinitions = schemaFile.attributes.filter(attrDef => attrDef.localName === attrName);

			if(!attrDefinitions.length)
				throw new Error(`The "${attrName}" attribute does not exist in this schema.`);

			return attrDefinitions.map((attrDefinition, index) => {
				var attrIndex = schemaFile.attributes.indexOf(attrDefinition);

				return {
					schemaFile: schemaFileName.substr(this._code.path.length + 1),
					localName: attrDefinition.localName,
					typeLocalName: attrDefinition.typeLocalName,
					typeNamespace: attrDefinition.typeNamespace,
					use: attrDefinition.use,
					defaultValue: attrDefinition.defaultValue,
					allowedValues: (attrDefinition.allowedValues || []).sort(),
					elements: arrayUnique(
						schemaFile.elements
							.filter(element => element.attributeRefs.indexOf(attrIndex) >= 0)
							.map(element => element.localName)
					).sort()
				};
			});
		}
	}

	app.registerAddon('schema', [SchemaAddon]);
};

function getElementSummary (schemaFile, nodeDefinition) {
	var flatContentModel = flattenContentModel(schemaFile.contentModels[nodeDefinition.contentModelRef]);

	return {
		localName: nodeDefinition.localName,
		documentation: nodeDefinition.documentation ? nodeDefinition.documentation.replace(/\s\s+/g, ' ') : undefined,
		isEmpty: !!flatContentModel[0] && flatContentModel[0].type === 'empty',
		isMixed: !!nodeDefinition.isMixed,
		isAbstract: !!nodeDefinition.isAbstract,
		contentModelString: transformContentModel(schemaFile.contentModels[nodeDefinition.contentModelRef]),
		containedBy: getNodeNamesReferringContentModel(schemaFile, nodeDefinition.localName)
			.map(function (element) {
				return element.localName;
			})
			.sort(),
		contains: arrayUnique(
			flatContentModel
				.filter(element => element.type === 'element')
				.map(function (element) {
					return element.localName;
				})
			)
			.sort(),
		attributes: nodeDefinition.attributeRefs
			.map(attributeRef => schemaFile.attributes[attributeRef])
			.sort()
	};
}

function hasContentModelForNodeName(nodeName, contentModel) {
	return contentModel.items ?
		contentModel.items.some(hasContentModelForNodeName.bind(undefined, nodeName)) :
		(contentModel.localName === nodeName);
}

function getContentModelRefFor(schema, nodeDef) {
	return schema.contentModels.reduce(function (accum, cm, i) {
		return hasContentModelForNodeName(nodeDef, cm)
			? accum.concat([i])
			: accum;
	}, []);
}

function getElementsReferringCM(schema, ref) {
	return schema.elements.reduce(function (accum, element) {
		return element.contentModelRef === ref
			? accum.concat([element])
			: accum;
	}, []);
}

function getNodeNamesReferringContentModel(schema, nodeDef) {
	return getContentModelRefFor(schema, nodeDef).reduce(function (accum, ref) {
		return accum.concat(getElementsReferringCM(schema, ref));
	}, []);
}

function flattenContentModel(contentModel) {
	return contentModel.items
		? contentModel.items.reduce(function (accum, item) {
				return accum.concat(flattenContentModel(item));
			}, [])
		: [contentModel];
}

function getCardinality(minOccurs, maxOccurs) {
	if (minOccurs === maxOccurs)
		return minOccurs === 1 ? '' : '{' + minOccurs + '}';

	return '{' + minOccurs + ',' + (maxOccurs || '…') + '}';
}

function transformContentModel(contentModel) {
	var cardinality = getCardinality(contentModel.minOccurs, contentModel.maxOccurs);
	switch (contentModel.type) {
		case 'element':
			return contentModel.localName + cardinality;
		case 'sequence':
			return '(' + contentModel.items
				.map(transformContentModel)
				.join(' ') + ')' + cardinality;
		case 'choice':
			return '(' + contentModel.items
				.map(transformContentModel)
				.join('|') + ')' + cardinality;
	}
}