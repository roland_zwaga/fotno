'use strict';

var path = require('path'),
	glob = require('globby');

module.exports = app => {
	app.registerAddon('operations', [
		OperationsJsonAddon
	]);
};

function OperationsJsonAddon (code) {
	this._code = code;
	this.path = code.path;
	this.operations = [];
}

function safeReadJson (path) {
	try {
		return require(path);
	} catch(e) {
		return false;
	}
}

OperationsJsonAddon.prototype.getOperations = function () {
	var workingDirectory = this.path;
	return glob(
			[
				'packages/**/operations*.json',
				'platform/**/operations*.json'
				],
			{
				cwd: workingDirectory
			}).then(operationsJsons => {
				return operationsJsons.reduce((allOps, operationsJson) => {
					var operations = require(path.join(workingDirectory, operationsJson));
					return allOps.concat(Object.keys(operations)
							.filter(operationName => !!operations[operationName])
							.map(operationName => {
								var operation = operations[operationName];
								operation.operationName = operationName;
								operation.operationFile = operationsJson;
								return operation;
							})
					);
				}, []);
		});
};

OperationsJsonAddon.prototype.init = function (code) {
};