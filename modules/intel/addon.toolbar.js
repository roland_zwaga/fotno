'use strict';

var path = require('path');
var jade = require('pug'); // previously called "jade", but now fuck you

var DOMParser = require('xmldom').DOMParser;
var XMLSerializer = require('xmldom').XMLSerializer;
const forEachDomNode = require('../../primitives/forEachDomNode');

function iterateOverNodesInFile(file, iterator) {

	// Load file
	return new Promise(function (resolve, reject) {
		fs.readFile(file, 'utf-8', function (err, xml) {
			return err ? reject(err) : resolve(new DOMParser().parseFromString(xml));
		});
	})

		// Apply transformation recursively on all nodes
		.then(function (doc) {
			function derp(parent) {
				iterator(parent);
				if (parent.childNodes)
					Object.keys(parent.childNodes).forEach(function (i) {
						derp(parent.childNodes[i]);
					});
			}

			derp(doc.documentElement);

			return doc;
		})

		// Export to file
		.then(function (doc) {
			return new Promise(function (resolve, reject) {
				fs.writeFile(file, new XMLSerializer().serializeToString(doc), 'utf-8', function (err) {
					return err ? reject(err) : resolve();
				});
			});
		});
}


module.exports = app => {
	app.registerAddon('toolbar', [
		ToolbarAddon
	]);
};

class ToolbarAddon {
	constructor (code) {
		this._code = code;
		this.toolbarPath = path.resolve(code.path, 'config', 'toolbar-template.jade');
		this.toolbarHtml = null;

	}

	init (code) {
	}

	getHtml () {
		if(!this.toolbarPath)
			throw new Error(`Code has no toolbar`);

		try {
			var createCustomJadeParserConstructor = require(path.join(this._code.path, 'node_modules', 'fontoxml-build-tools', 'src', 'createCustomJadeParserConstructor')),
				customJadeParserContructor = createCustomJadeParserConstructor(this._code.path);
		} catch (e) {
			throw new Error('Could not use Jade parser from node_modules, try running "npm i" first');
		}

		// Load file
		if(!this.toolbarHtml)
			this.toolbarHtml = jade.compileFile(this.toolbarPath, {
				parser: customJadeParserContructor
			})();
		return this.toolbarHtml;
	}

	getDom () {
		//console.log(this.getHtml());
		return new DOMParser().parseFromString(this.getHtml());
	}

	getToolbarTabs () {
		var tabs = [];
		forEachDomNode(this.getDom(), node => {
			if(node.nodeName !== 'ui-tab')
				return;
			tabs.push({
				id: node.recurseIntoNoderecurseIntoNodegetAttribute('tab-id'),
				label: node.getAttribute('label'),
				ifCursor: node.getAttribute('visible-if-cursor-in'),
				ifSx: node.getAttribute('visible-if-schema-experience-has-module')
			});
		});
		return tabs;
	}

	getHierarchy () {
		const root = {
			type: 'ui-toolbar',
			children : []
		};
		const parents = [
			'fonto-operation',
			'ui-quick-access-buttons',
			'ui-tab',
			'ui-drop',
			'ui-button',
			'ui-button-group',
			'ui-button-container',
			'ui-multi-button',
			'ui-menu-item',
			'ui-menu-multi-item',
			'ui-split-drop',
			'ui-grid-size-selector',
			'ui-special-character-grid'
		];
		function smurfNodeRecursive (root, dom) {

			if(parents.indexOf(dom.nodeName) >= 0) {
				let newParent = {
					type: dom.nodeName,
					children: []
				};

				if(dom.nodeName === 'fonto-operation') {
					newParent.operationName = dom.getAttribute('name');
				}

				if (dom.hasAttribute('label'))
					newParent.label = dom.getAttribute('label') || '';

				if (dom.hasAttribute('ui-tooltip-content'))
					newParent.tooltipContent = dom.getAttribute('ui-tooltip-content') || '';

				root.children.push(newParent);

				if(dom.childNodes)
					Object.keys(dom.childNodes).forEach(i => smurfNodeRecursive(newParent, dom.childNodes[i]));
			} else {

				if(dom.childNodes)
					Object.keys(dom.childNodes).forEach(i => smurfNodeRecursive(root, dom.childNodes[i]));
			}
		}
		smurfNodeRecursive (root, this.getDom().documentElement);
		return root;
	}
}
