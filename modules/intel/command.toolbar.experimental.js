'use strict';

const path = require('path');

function mapForLength(n, mapper) {
	var result = [];
	for (var i = 0; i < n; ++i) {
		result.push(mapper(i));
	}
	return result;
}
function toolbarToJson (hierarchy) {
	return JSON.stringify(hierarchy, null, '\t');
}

function toolbarToFlatList (hierarchy, operations) {
	var recursionLevel = 0,
		recurseFn = (rows, node) => {
			node.depth = recursionLevel;
			if(node.operationName && operations[node.operationName]) {
				let operation = operations[node.operationName];
				node.label = node.label || operation.label;
				node.description = node.description || operation.description;
				node.icon = node.icon || operation.icon;
			}
			rows.push(Object.assign({}, node, { children: null }));
			if(node.children) {
				++recursionLevel;
				rows = node.children.reduce(recurseFn, rows);
				--recursionLevel;
			}
			return rows;
		};
		return hierarchy.children.reduce(recurseFn, []);
}

function toolbarToTableStrings (hierarchy, operations) {
	var rowData = toolbarToFlatList(hierarchy, operations),
		maxRecursionLevel = rowData.reduce((max, item) => item.depth > max ? item.depth : max, 0);

	return rowData.map((item, index) => {
		return mapForLength(item.depth, () => '')
		.concat([item.type])
		.concat(mapForLength(maxRecursionLevel - item.depth, () => ''))
		.concat([
			item.operationName ? item.operationName : '',
			item.label ? item.label : item.label !== undefined ? '(Override empty)' : '',
			item.description ? item.description : item.description !== undefined ? '(Override empty)' : '',
			item.icon ? item.icon : item.icon !== undefined ? '(Override empty)' : ''
		])
	})
}
module.exports = app => {
	app.cli.addCommand('toolbar')
		.setDescription('(Experimental) Summarize operations in the toolbar. Parses the toolbar template and operations to construct a layout of the toolbar in a spreadsheet.')
		.addOption('export', false, 'Print the result to a certain format (json|csv|gs|inspect*)')
		.setController((req, res) => {
			return Promise.all(req.scope.map(code => {
				return code.getAddon('operations').getOperations()
					.then(allOperations => {
						var uniqueOperations = allOperations.reduce((unique, op) => {
							if(!unique[op.operationName])
								unique[op.operationName] = op;
							return unique;
						}, {});

						switch(req.options.export) {
							case 'json':
								console.log(toolbarToJson(code.getAddon('toolbar').getHierarchy(), uniqueOperations));
								break;

							case 'gs':
								console.log(JSON.stringify(toolbarToFlatList(code.getAddon('toolbar').getHierarchy(), uniqueOperations), null, '\t'));
								break;

							case 'csv':
								console.log(toolbarToTableStrings(code.getAddon('toolbar').getHierarchy(), uniqueOperations).map(row => row.join('\t')).join('\n'));
								break;

							case 'inspect':
							default:
								console.dir(code.getAddon('toolbar').getHierarchy(), {
									depth: 6,
									colors: true
								});
						}
					})
					.catch(console.log.bind(console));
			}));
		})
};