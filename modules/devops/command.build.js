'use strict';

const path = require('path');

module.exports = app => {

	// $ fotno install
	app.cli.addCommand('build')
		.setDescription(`Use the official FontoXML tools to build the application. Runs the equivalent of "npm run build"`)
		.setController((req, res) => {
			var code = req.scope[0];

			res.caption(`fotno build`);
			res.properties({
				'Application': code.getAddon('identity').name,
				'SDK version': code.getAddon('identity').sdkVersion,
				'Path': code.path
			});
			
			var emitter = code.getAddon('sdk').executeBuildCommand();

			res.caption('Build output');
			emitter.on('log', data => res.debug(('' + data).trim()));
			emitter.on('error', data => {
				res.break();
				res.error(('' + data).trim());
			});

			emitter.on('close', function () {
				res.break();
				res.debug('End of build task');
				res.break();
			})
		});
};