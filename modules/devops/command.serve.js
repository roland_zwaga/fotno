'use strict';

const path = require('path');
const AskNicely = require('ask-nicely');

function makeServerConfig (options, res) {
	return {
		port: parseInt(options.port),
		savemode: options.save,
		documentLoadLock: options.lock,
		delay: options.delay,
		dist: !!options.dist,
		verbose: !!options.verbose,
		open: !!options.open
	};
}

module.exports = app => {
	function serveApplicationController (req, res) {
		var code = req.scope[0];

		res.caption(`fotno serve`);
		res.properties({
			'Application': code.getAddon('identity').name,
			'SDK version': code.getAddon('identity').sdkVersion,
			'Path': code.path
		});

		var emitter = code.getAddon('sdk').executeServerCommand(makeServerConfig(req.options));

		res.caption('Server output');
		emitter.on('log', data => res.debug(('' + data).trim()));
		emitter.on('error', data => {
			res.break();
			res.error(('' + data).trim());
		});

		emitter.on('close', function () {
			res.break();
			res.debug('End of server process');
			res.break();
		})
	}

	app.cli.addCommand('serve')
		.setDescription(`Use the official FontoXML tools to serve the application. Runs the equivalent of "npm run server", but has different defaults and a more lenient syntax.`)
		.setLongDescription(`This command uses the version of fontoxml-dev-server and fontoxml-build-tools packaged with your FontoXML application.`)
		.addExample(`fotno serve`, `Start the development server on port 8080, without savemode`)
		.addExample(`fotno serve -ovsp 9999 --dist`, `Start the build server on port 9999 with savemode=disk and open in the browser right away.`)
		.setController(serveApplicationController)
		.addOption('open', 'o', 'Open a new browser tab for eachserved application', false)
		.addOption('dist', 'P', 'Serve from dist/ folder', false)
		.addOption('verbose', 'v', 'Verbose, not sure what that means tho', false)
		.addOption(new AskNicely.Option('port')
			.setShort('p')
			.setDescription('Port number to serve the application on, defaults to 8080')
			.setDefault(8080, true)
			.setResolver(parseInt)
		)
		.addOption(new AskNicely.Option('delay')
			.setShort('d')
			.setDescription('Delay in milliseconds for CMS routes, defaults to 0 if omitted or 3000 if left empty.')
			.setDefault(3000, false)
			.setResolver(val => {
				val = parseInt(val);
				return (!val || isNaN(val)) ? 0 : val;
			})
		)
		.addOption(new AskNicely.Option('save')
			.setShort('s')
			.setDescription('Save mode, defaults to "off" if omitted or "disk" if empty. Must be one of off|disk|session.')
			.setDefault('disk', false)
			.setResolver(val => {
				if(!val)
					val = 'off';

				if('disk|session|off'.split('|').indexOf(val) === -1)
					throw new AskNicely.InputError(`"${val}" is not a valid save mode, must be one of disk|session|off.`)

				return val;
			})
		)
		.addOption(new AskNicely.Option('lock')
			.setShort('l')
			.setDescription('Lock status, defaults to "ok" if omitted or "na" if left empty. Must be one of ok|nav|naq|na.')
			.setDefault('na', false) // this is the default value if the -l flag is used but empty
			.setResolver(val => {
				switch (val) {
					case undefined: // this is the default value if the -l flag is *not* used
					case '1':
					case 'true':
					case 'ok':
						return { isLockAvailable: true, isLockAcquired: true };
					case 'nav':
						return { isLockAvailable: false, isLockAcquired: true };
					case 'naq':
						return { isLockAvailable: true, isLockAcquired: false };
					case '0':
					case 'false':
					case 'na':
						return { isLockAvailable: false, isLockAcquired: false };
					default:
						throw new AskNicely.InputError(
							`"${val}" is not a valid lock option, must be one of ok|nav|naq|na.`,
							`Must be either "ok", "nav", "naq" or "na". To help you remember, "nav" = Not islockAVailable, "naq" = Not islockAcQuired, "na" = Not Anything`
						);
				}
			})
		);
};