'use strict';

const glob = require('globby');
const EventEmitter = require('events').EventEmitter;
const path = require('path');

module.exports = app => {
	app.registerAddon('cms', [
		CmsAddon
	]);
};

class CmsAddon extends EventEmitter {
	constructor (code) {
		super();
		this.paths = [
			path.join(code.path, 'dev-cms', 'uploads'),
			path.join(code.path, 'dev-cms', 'files')
		];
	}

	init () {
	}

	getFileList () {
		return Promise.all(this.paths.map(p => glob(['**/*'], { cwd: p })))
			.then(filesPathPath => {
				console.log(filesPathPath);
				var fileMap = {};

				filesPathPath.forEach(files => files.forEach(file => {
					if(fileMap[file])
						return Object.assign(fileMap[file], { isChanged: true});

					fileMap[file] = {
						path: file,
						isChanged: false
					};
				}));

				return fileMap;
			})
	}
}