'use strict';

const AskNicely = require('ask-nicely');
const path = require('path');
const os = require('os');
const fs = require('fs-extra');

const FormData = require('form-data');
const unzip = require('unzip2');

const getAllStatsInPath = require('../../primitives/getAllStatsInPath');

function promiseRemove(p) {
	return new Promise((res, rej) => {
		fs.remove(p, err => err ? rej(err) : res());
	});
}

function promiseMove (p, p2, options) {
	return new Promise((res,rej) => {
		fs.move(p, p2, options || {}, err => err ? rej(err) : res())
	});
}

module.exports = app => {

	// $ fotno install
	app.cli.addCommand('upgrade')
		.setDescription(`Upgrade an application to the latest SDK.`)
		.addOption('examples', 'e', 'Also upgrade the examples included with the SDK')
		.setController((req, res) => {
			res.caption(`fotno upgrade`);

			let code = req.scope[0],
				manifestPath = path.resolve(code.path, 'manifest.json'),
				manifest = require(manifestPath),
				temporaryPath = path.resolve(code.path, 'upgrade'),
				temporaryArchivePath = path.join(temporaryPath, '_download.zip'),
				allowedBaseFolders = ['manifest.json', 'tools', 'platform', 'examples'];

			let form = new FormData();

			form.append('manifest', fs.createReadStream(manifestPath));

			Object.keys(manifest).forEach(fieldName => {
				let val = manifest[fieldName];

				val && form.append(fieldName, Array.isArray(val) ? JSON.stringify(val) : val);
			});

			if(!app.config.sdk)
				throw new AskNicely.InputError(
					`The upgrade command requires a username and password to sdk.fontoxml.com to be set in the "sdk" config variable.`,
					`Create a file ".fotnorc" in your home directory (${os.homedir()}) or an ancestor directory of this application. The config file (JSON) should contain a object "sdk" with the "username" and "password" properties.`
				);

			return new Promise((resolve, reject) => {
					res.debug('Sending upgrade request to sdk.fontoxml.com');
					form.submit({
						host: 'sdk.fontoxml.com',
						path: '/api/upgrade-platform',
						auth: app.config.sdk.username + ':' + app.config.sdk.password
					}, function (err, response) {
						if (err)
							return reject(err);

						if (response.statusCode !== 200)
							return reject(new Error('sdk.fontoxml.com responded with a non-200 status code: ' + response.statusCode));

						fs.ensureDirSync(temporaryPath);

						res.debug('Downloading upgrade...');

						const fd = fs.openSync(temporaryArchivePath, 'w'),
							chunks = [];

						response.on('data', chunk => {
							chunks.push(chunk);
						});

						response.on('end', () => {
							res.debug('Writing archive to a temporary location');
							chunks.forEach(chunk => fs.writeSync(fd, chunk, 0, chunk.length, null));
							fs.closeSync(fd);
							resolve();
						});
					});
				})
				.then(() => {
					res.debug('Extracting archive to a temporary location');
					let hasError = false;

					return new Promise((resolve, rej) => fs.createReadStream(temporaryArchivePath)
						.pipe(unzip.Parse())
						.on('entry', function (entry) {
							var baseFolder = entry.path.split('/')[0];

							if (hasError || entry.type === 'Directory') {
								entry.autodrain();
								return;
							}

							if (allowedBaseFolders.indexOf(baseFolder) === -1) {
								entry.autodrain();
								return;
							}

							fs.ensureDirSync(path.dirname(path.join(temporaryPath, entry.path)));

							entry.pipe(fs.createWriteStream(path.join(temporaryPath, entry.path)));
						})
						.on('error', err => {
							if(hasError) {
								return;
							}
							hasError = true;

							res.debug('Removing temporary files because an error was encountered');
							return promiseRemove(temporaryPath).then(() => rej(new AskNicely.InputError(
								'Could not extract the upgrade archive: ' + err.message,
								'Unfortunately an error at this stage happens quite frequently. Simply retry the upgrade command (it might work the next time) or perform a manual upgrade.'
							)));
						})
						.on('close', () => resolve()));
				})
				.then(() => {
					res.debug('Removing platform/ from the application repository');
					return promiseRemove(path.resolve(code.path, 'platform'));
				})
				.then(() => {
					res.debug('Removing tools/ from the application repository');
					return promiseRemove(path.resolve(code.path, 'tools'));
				})
				.then(() => {
					res.debug('Copying over extracted upgrade files into repository');
					allowedBaseFolders.splice(allowedBaseFolders.indexOf('examples'), 1);
					return Promise.all(allowedBaseFolders.map(baseFolder => promiseMove(
						path.resolve(code.path, 'upgrade', baseFolder),
						path.resolve(code.path, baseFolder),
						{
							clobber: true
						})));
				})
				.then(()=> {
					if(!req.options.examples)
						return;

					res.debug('Copying over example SX configuration');

					let exampleDirs = [];

					try {
						exampleDirs = getAllStatsInPath.sync(path.resolve(code.path, 'upgrade', 'examples'))
							.filter(stat => stat.directory)
							.map(stat => stat.path);
					} catch (e) {
						res.error('Could not copy over examples: ' + e.message);
						return;
					}

					return Promise.all(exampleDirs.map(exampleDir => {
							return promiseRemove(path.resolve(code.path, 'packages', path.basename(exampleDir)));
						}))
						.then(() => Promise.all(exampleDirs.map(exampleDir => {
							return promiseMove(exampleDir, path.resolve(code.path, 'packages', path.basename(exampleDir)))
						})));
				})
				.then(() => {
					res.debug('Removing temporary files');
					return promiseRemove(temporaryPath);
				})
				.then(() => {
					res.success('Done');
					res.break();

					res.log('Please review the release notes for this SDK version to find out if there are manual changes you should make.');
				});
		});
};