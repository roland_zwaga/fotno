'use strict';

const EventEmitter = require('events').EventEmitter;
const spawn = require('child_process').spawn;
const os = require('os');
const path = require('path');
const fs = require('fs-extra');
const gitDownload = require('../../primitives/gitDownload');

module.exports = app => {
	app.registerAddon('sdk', [
		'identity',
		ServerAddon
	]);
};

function execute(cwd, argv) {
	var eventEmitter = new EventEmitter(),
		spawnedProcess = spawn(argv[0], argv.splice(1), {
			cwd: cwd
		});

	spawnedProcess.stdout.on('data', (data) => {
		eventEmitter.emit('log', data);
	});

	spawnedProcess.stderr.on('data', (data) => {
		eventEmitter.emit('error', data);
	});

	spawnedProcess.on('close', (code) => {
		eventEmitter.emit('close', code);
		eventEmitter.removeAllListeners();
	});

	return eventEmitter;
}

class ServerAddon extends EventEmitter {
	constructor (code) {
		super();
		this.config = {
			root: code.path,
			savemode: 'disk',
			port: 8080,
			documentLoadLock: {
				isLockAcquired: true,
				isLockAvailable: true
			}
		};
	}

	init (code, identity) {
		this.config = Object.assign(this.config, identity.config);
	}
	/**
	 * Compatible with SDK >= 6.1.0
	 * @param config
	 */
	executeServerCommand (config) {
		var concatenatedConfig = Object.assign({}, this.config, config);

		let args = ['node', './node_modules/fontoxml-dev-server/bin/fontoxml-dev-server'];

		args = args.concat(['--port', concatenatedConfig.port]);

		if(concatenatedConfig.dist)
			args = args.concat(['--dist']);

		if(concatenatedConfig.open)
			args = args.concat(['--open']);

		if(concatenatedConfig.delay)
			args = args.concat(['--delay', concatenatedConfig.delay]);

		if(concatenatedConfig.savemode)
			args = args.concat(['--savemode', concatenatedConfig.savemode]);

		if(concatenatedConfig.documentLoadLock && !concatenatedConfig.documentLoadLock.isLockAvailable)
			args = args.concat(['--lock-not-available']);

		if(concatenatedConfig.documentLoadLock && !concatenatedConfig.documentLoadLock.isLockAcquired)
			args = args.concat(['--lock-not-acquired']);

		if(concatenatedConfig.verbose)
			args = args.concat(['--verbose']);

		return execute(concatenatedConfig.root, args);
	}


	/**
	 * Compatible with SDK >= 6.1.0
	 */
	executeBuildCommand (config) {
		return execute(
			Object.assign({}, this.config, config).root,
			['node', './node_modules/fontoxml-build/bin/fontoxml-build']);
	}

	getTagsForVersion (releaseTag) {
		var tmpDir = path.resolve(os.tmpdir(), 'fontoxml-release-' + (new Date().getTime()));

		return gitDownload({
			source: `ssh://git@bitbucket.org/liones/fontoxml-release.git`,
			dest: tmpDir,
			branch: releaseTag
		})
			.then(() => {
				var it = null;
				try {
					it = fs.readJsonSync(path.join(tmpDir, 'fonto-manifest.json')).dependencies
				} catch (e) {}

				if(!it)
					try {
						var deps = fs.readJsonSync(path.join(tmpDir, 'bower.json')).dependencies;
						return Object.keys(deps).reduce((clean, name) => {
							clean[name] = deps[name].substr(deps[name].indexOf('#') + 1);
							return clean;
						}, {})
					} catch (e) {}

				fs.removeSync(tmpDir);

				if (it) return it;

				throw new Error('Could not extract package tags from fontoxml-release#' + releaseTag);
			});
	}
}