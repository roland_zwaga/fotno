# How-to

__Install__

```
$ npm i ssh://git@bitbucket.org:wvbe/fotno -g
```

Note that commands that use sdk.fontoxml.com require a username and password to be configured through `.fotnorc`, see
below. Other commands require access to Fonto's repositories, which can not be configured but only be earned with hard
fought tough love, blood, sweat and tears.

__Use__

Give a man a fish and he eats for a day. Teach a man to fish and he eats for a lifetime. Use the `--help` flag or try
the crash course below.

```
$ fotno command [parameters] [--options]
```

__Update__

```
$ npm update fotno -g
```

__Develop__

Check out the files in `modules`. Any Javascript file in there is automatically included (this will change in a later
version). In short: If you add commands you can give them a controller callback, which will be called with 2 arguments:
the `request` object, and the `result` object (instance of [speak-softly](https://github.com/wvbe/speak-softly)).
Asynchronous controllers are expected to return a Promise. For a more accurate and complete idea of what you can do,
check out the docs for [ask-nicely](https://npmjs.org/repos/ask-nicely).

```
$ git clone git@bitbucket.org:wvbe/fotno.git
$ cd fotno
$ npm link
```

# .fotnorc
Currently you can configure your credentials to sdk.fontoxml.com, output colors and the globbing pattern for modules to
load (eg. if you'd rather not load experimental features). Place a file called `.fotnorc` in

- your current working directory or a parent directory of it
- or in your home directory.

```
{
  "sdk": {
    "username": "xXFontoBossXx",
    "password": "f0nt0b055"
  },
  "colors": {
    "caption": ["bold"],
    "notice": ["yellow", "underline"]
  }
}
```

The color config object is passed to [speak-softly](https://github.com/wvbe/speak-softly).

# Crash course

Since there is `-h` it is no use explaining the particulars of individual commands etc. Here's a couple of real-life
examples to illustrate the particulars of the syntax.

`fotno` is always the first thing you type. The words after that are the child commands, parameters and options. The
order of commands and parameters is important, but options can go anywhere. Most options have a one-letter alternative,
which you can bunch together using a single "-" prefix.

```
$ fotno serve
$ fotno serve --help
$ fotno serve --port 8081 --lock --save --dist -v
$ fotno serve -svlPp 8081
```

The schema commands from previous versions (`element`, `attribute` and `elements`) are still here, but the options have
more unicorn. In an application with only one `schema.json` you can leave out the `--schema` option. If you use them in
an application with two or more schema files you just need to specify an unique part of its name. The old way still
works for compatibility with editors that have renamed their `schema.json`.

```
$ fotno element body
$ fotno element body --schema top
$ fotno element body -s packages/all-my-derp-schema/src/shells/nerf.json
```

A few commands give you tabular data: `operations` and `elements`. The options to handle table data are standardized so
you can find/export what you need.

```
$ fotno operations --columns name file label desc key
$ fotno elements --columns name label desc --sort desc --export
```

# Final notes

- There are two aliases for official FontoXML tools included with fotno for conveinience: `serve` and `build`. They
  require the node_modules of your FontoXML editor to be installed.
  
- Pull-requests for new commands etc. are encouraged but more urgently a way of injecting modules based on user- or
  application preferences is required.
  
- There is no support for old SDK versions, so if your app is severely outdated or breaks a lot of code conventions you
  may have to fix that first. Generally speaking fotno should play nice with SDK 6.1.0 < x < 7.0.0.