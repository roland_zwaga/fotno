'use strict';

const ADDONS = Symbol('addons');
class AddonHost {
	constructor () {
		this[ADDONS] = {};
	}

	getAddon (name) {
		return this[ADDONS][name];
	}

	setAddon (name, addonClass) {
		this[ADDONS][name] = addonClass;
	}

	getAddonNames () {
		return Object.keys(this[ADDONS]);
	}

	init (addons) {
		if (!addons)
			return Promise.resolve(this);

		return new Promise((resolve, reject) => {
			bootAddons(
				this,
				Object.keys(addons).reduce((addns, name) => {
					var addonDescription = addons[name];
					addonDescription = Array.isArray(addonDescription) ? addonDescription : [addonDescription];

					var Addon = addonDescription[addonDescription.length - 1],
						dependencies = addonDescription.length > 1 ? addonDescription.slice(0, addonDescription.length - 1) : [];

					addns[name] = [dependencies, Addon];

					return addns;
				}, {}),
				this.getAddonNames(),
				[],
				err => err ? reject(err) : resolve(this)
			);
		}).then(code => {
				return code;
		});
	}
}
function bootAddons (parsee, addonsKnown, addonsDone, addonsBusy, whenDone) {
	var addonsWaiting = Object.keys(addonsKnown)
			.filter(parserName => {
				return addonsDone.indexOf(parserName) === -1 && addonsBusy.indexOf(parserName) === -1;
			}),

	// Select parsers who have no unresolved dependencies
		addonsOpen = addonsWaiting.filter(parserName => {
			return addonsKnown[parserName][0].every(parserDependencyName => {
				return addonsDone.indexOf(parserDependencyName) >= 0;
			});
		});

	// If it seems there's nothing left to do...
	if (!addonsOpen.length) {
		if(!addonsBusy.length && addonsWaiting.length) {
			return whenDone(new Error('There was a problem resolving all dependencies, some addons could not be initialized: ' + addonsWaiting.join(', ')));
		}
		return whenDone(null);
	}

	// For every addon that is ready to be initialized, start a new asynch thread
	addonsOpen.forEach(addonName => {
		// Instantiate and save in parent AddonHost
		var addon = new addonsKnown[addonName][1](parsee);

		parsee.setAddon(addonName, addon);

		// Mark as busy resolving
		addonsBusy.push(addonName);

		// Treat all init() functions as a promise eventually, call init() with
		// the parent AddonHost and all instantiated dependencies as arguments
		Promise.resolve(
			addon.init.apply(
				addon,
				[parsee].concat(addonsKnown[addonName][0].map(function (dependencyName) {
					return parsee.getAddon(dependencyName);
				}))
			)
		)
			.then(function () {
				// Unmark as resolving, mark as resolved
				addonsBusy.splice(addonsBusy.indexOf(addonName), 1);
				addonsDone.push(addonName);
				// If all addons are now resolved, end the asyncness. This should happen only once.
				if(addonsDone.length === Object.keys(addonsKnown).length)
					return whenDone(null);

				// If not everything is either resolved or resolving, run the whole show all over again
				if(addonsDone.length + addonsBusy.length < Object.keys(addonsKnown).length)
					bootAddons(parsee, addonsKnown, addonsDone, addonsBusy, whenDone);
			})
			.catch(function (error) {
				whenDone(error);

				// Might occur multiple times, so take out the whenDone function since we
				// shouldn't use it any more.
				whenDone = function () {};
			});
	});
}

module.exports = AddonHost;