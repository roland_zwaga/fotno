'use strict';

// core node modules
const os = require('os');
const path = require('path');

// 3rd party node modules
const glob = require('globby');
const AskNicely = require('ask-nicely');
const SpeakSoftly = require('speak-softly');

// classes, helpers
const Code = require('./Code');
const FotnoCommand = require('./FotnoCommand');
const ConfigManager = require('./ConfigManager');

const getParentDirectoryContainingFile = require('../primitives/getParentDirectoryContainingFile');

class fotno {
	constructor (processPath, name) {
		this.name = name;

		this.processPath = processPath;

		this.config = new ConfigManager(getParentDirectoryContainingFile.sync(processPath, '.fotnorc'));

		this.logger = new SpeakSoftly(this.config.colors || {}, {
			indentation: '  '
		});

		this.cli = new AskNicely(this.name);
		this.cli.setNewChildClass(FotnoCommand);

		requireForGlobbingPatterns(this.config.modules, path.resolve(__dirname, '..'))
			.forEach(module => (typeof module === 'function') && module(this));
	}

	run (argv) {
		return this.cli.interpret(Object.assign([], argv), null, this.logger)
			.then(request => request.execute(this.logger))

			.catch(err =>  {
				this.error('fotnofail', err, {
					argv: [this.name].concat(argv.map(arg => arg.indexOf(' ') >= 0 ? `"${arg}"` : arg)).join(' '),
					cwd: this.processPath
				});

				// Do not hard exit program, but rather exit with error code one the program is closing itself
				process.on('exit', function () {
					process.exit(1);
				});
			})

			.then(() => {
				os.platform() !== 'win32' && this.logger.break();
				return this;
			});
	}

	error (caption, err, debugVariables) {
		if(caption)
			this.logger.caption(caption);

		if(err)
			this.logger.error(err.message || err.stack || err);
		else
			console.trace('Empty error');

		if (err instanceof AskNicely.InputError)
			this.logger.log(err.solution || 'You might be able to fix this, use the "--help" flag for usage info');
		else {
			this.logger.debug(err.stack);

			if (debugVariables)
				this.logger.properties(debugVariables);
		}

	}

	// @TODO: Remove this
	registerAddon (name, definition) {
		return Code.registerAddon(name, definition);
	}
}

function requireForGlobbingPatterns (patterns, cwd) {
	return glob.sync(patterns, { cwd: cwd })
		.reduce((modules, modulePath) => {
			let mod = require(path.resolve(cwd, modulePath));
			if(typeof mod === 'function')
				modules.push(mod);

			return modules;
		}, []);
}

module.exports = fotno;