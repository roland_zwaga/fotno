'use strict';

const os = require('os');
const path = require('path');
const fs = require('fs-extra');

const bootstrapConfiguration = require('../config/bootstrap.json');

const LOCATION = Symbol('configuration file location');
class ConfigManager {
	constructor (configPathOverride) {
		Object.assign(this, bootstrapConfiguration);

		try {
			Object.assign(this, require(`../config/bootstrap-${os.platform()}.json`));
		} catch (e) {
			// There is no config specific to user platform, no biggie
		}

		this[LOCATION] = configPathOverride || (fs.existsSync(path.resolve(os.homedir(), '.fotnorc')) ? os.homedir() : null);

		if(this[LOCATION]) {
			try {
				this.read();
			} catch (e) {
				console.log('Error reading .fotnorc', e.stack);
			}
		}
	}

	read () {
		Object.assign(this, fs.readJsonSync(path.resolve(this[LOCATION], '.fotnorc')));
	}

	getPath () {
		return this[LOCATION] || null;
	}


	save () {
		if(!this[LOCATION])
			return Promise.reject(new Error(`Location for config file is undefined`));

		return new Promise((res, rej) => fs.outputFile(this[LOCATION], this, err => err
				? rej(err)
				: res()
		));
	}

	toString () {
		return JSON.stringify(Object.keys(this).reduce((sanitized, propertyName) => {
			if (propertyName.charAt(0) !== '_' && typeof this[propertyName] !== 'function')
				sanitized[propertyName] = this[propertyName];
			return sanitized;
		}, {}), null, '\t');
	}
}

module.exports = ConfigManager;