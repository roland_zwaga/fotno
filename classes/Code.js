'use strict';

const fs = require('fs'),
	path = require('path'),
	AddonHost = require('./AddonHost');


var ADDONS = {};

class Code extends AddonHost {
	constructor (manifestLocation) {
		super();

		this.path = manifestLocation;
	}

	existsOnDisk () {
		try {
			return !!fs.statSync(this.path);
		} catch (e) {
			return false;
		}
	}

	static registerAddon (name, addonSpec) {
		ADDONS[name] = addonSpec;
	};


	// This is fucked
	static getAddonSubset (addons) {
		var addonRegistry = ADDONS;

		return (function addDependenciesToList(list, addonNames) {
			if(!addonNames || !addonNames.length)
				return list;

			var newAddons = addonNames.filter(function (addonName) {
				return list.indexOf(addonName) === -1;
			});

			list = list.concat(newAddons);

			list = addDependenciesToList(list, newAddons.reduce(function (depList, depName) {
				if(!addonRegistry[depName])
					throw new Error(`Addon "${depName}" is not registered`);
				return depList.concat(typeof addonRegistry[depName][0] === 'function'
					? []
					: addonRegistry[depName].filter(function (s) {
						return typeof s === 'string';
					}));
			}, []));

			return list;
		})([], addons ? (Array.isArray(addons) ? addons : [addons]) : [])
			.reduce((depObj, depName) => {
				depObj[depName] = addonRegistry[depName];
				return depObj;
			}, {});
	}
}

module.exports = Code;