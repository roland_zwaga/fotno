var DOMParser = require('xmldom').DOMParser;

function iterateOverNodesInFile(doc, iterator) {
	if(typeof doc === 'string')
		doc = new DOMParser().parseFromString(doc);

	function recurseIntoNode(parent) {
		if (iterator(parent) !== false && parent.childNodes)
			Object.keys(parent.childNodes).forEach(i => recurseIntoNode(parent.childNodes[i]));
	}

	recurseIntoNode(doc.documentElement);
}

module.exports = iterateOverNodesInFile;