'use strict';

const path= require('path');

const fs = require('fs-extra');

const getAllStatsInPath = require('./getAllStatsInPath');

module.exports = function getParentDirectoryContainingFile (cwd, configFileName) {

	const rootDir = path.parse(cwd).root;
	return getAllStatsInPath(cwd, true)
		.then(stats => stats.find(stat => stat.file && path.basename(stat.path) === configFileName)
			? cwd
			: cwd === rootDir // @TODO: Test dat shit like I give a shit
				? null
				: getParentDirectoryContainingFile(path.dirname(cwd), configFileName)
	);
};

module.exports.sync = function getParentDirectoryContainingFileSync(cwd, configFileName) {
	const rootDir = path.parse(cwd).root;
	return getAllStatsInPath.sync(cwd, false).find(stat => stat.file && path.basename(stat.path) === configFileName)
		? cwd
		: cwd === rootDir // @TODO: Test dat shit like I give a shit
			? null
			: getParentDirectoryContainingFileSync(path.dirname(cwd), configFileName)
};