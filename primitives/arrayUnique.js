function arrayUnique (arr){
	if(!arr) return [];
	var n = {}, r=[];
	for(var i = 0; i < arr.length; i++) {
		if (n[arr[i]])
			continue;
		n[arr[i]] = true;
		r.push(arr[i]);
	}
	return r;
}

module.exports = arrayUnique;