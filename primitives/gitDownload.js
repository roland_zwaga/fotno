'use strict';
const os = require('os');

const GitDownload = require('git-download');

module.exports = function gitDownload(downloadOptions) {
	return new Promise((resolve, reject) => GitDownload(
		Object.assign({
			tmpDir: os.tmpdir()
		}, downloadOptions),
		(err, data) => err ? reject(err) : resolve(data))
	);
};